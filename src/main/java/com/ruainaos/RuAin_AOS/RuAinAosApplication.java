package com.ruainaos.RuAin_AOS;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.ruainaos.RuAin_AOS.Models.Roles;
import com.ruainaos.RuAin_AOS.Repositories.RoleRepository;




@SpringBootApplication
public class RuAinAosApplication {

	public static void main(String[] args) {
		SpringApplication.run(RuAinAosApplication.class, args);
	}
	

	@Bean
	CommandLineRunner init(RoleRepository roleRepository) {

	    return args -> {

	        Roles adminRole = roleRepository.findByRole("ADMIN");
	        if (adminRole == null) {
	        	Roles newAdminRole = new Roles();
	            newAdminRole.setRole("ADMIN");
	            roleRepository.save(newAdminRole);
	        }

	        Roles userRole = roleRepository.findByRole("USER");
	        if (userRole == null) {
	        	Roles newUserRole = new Roles();
	            newUserRole.setRole("USER");
	            roleRepository.save(newUserRole);
	        }
	        
	   
	    };
	    
//	    if (!is_dir('path/to/directory')) {
//	        mkdir('path/to/directory', 0777, true);
//	    }

	}
	
	


}
