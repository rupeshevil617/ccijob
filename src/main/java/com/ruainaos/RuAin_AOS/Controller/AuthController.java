package com.ruainaos.RuAin_AOS.Controller;


import static org.springframework.http.ResponseEntity.ok;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruainaos.RuAin_AOS.Models.AdvertisingModel;
import com.ruainaos.RuAin_AOS.Models.Applicants;
import com.ruainaos.RuAin_AOS.Models.Jobs;
import com.ruainaos.RuAin_AOS.Models.Roles;
import com.ruainaos.RuAin_AOS.Models.User;
import com.ruainaos.RuAin_AOS.Repositories.UserRepository;
import com.ruainaos.RuAin_AOS.Security.JwtTokenProvider;
import com.ruainaos.RuAin_AOS.Service.AdvertisementService;
import com.ruainaos.RuAin_AOS.Service.CustomUserDetailsService;
import com.ruainaos.RuAin_AOS.Service.JobService;





@CrossOrigin( origins = "*" )

@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	String applyjobss="/var/tmp/";
//	String applyjobss=System.getProperty("user.dir") + "/uploadingDir/" ;
	


	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtTokenProvider jwtTokenProvider;

	@Autowired
	UserRepository users;
	
	@Autowired
	private AdvertisementService advertisementService;
	
	@Autowired
	private JobService jobService;
	
	Map<Object, Object> model = new HashMap<>();
	
	 String rootPath = System.getProperty("user.dir");



	@Autowired
	private CustomUserDetailsService userService;

	@SuppressWarnings("rawtypes")
	@PostMapping("/login")
	public ResponseEntity login(@RequestBody AuthBody data) {
		try {
			String email = data.getEmail();
			System.out.println("authentication vandamathi    "+email);
			//AUthenticator and authenticate is used to check whether the username or passwword march with database value or not
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, data.getPassword()));
			// if the all the data matches then it will create an token  which user can used for doing others transactions
			try {
				String token = jwtTokenProvider.createToken(email, this.users.findByEmail(email).getRoles());
				Map<Object, Object> model = new HashMap<>();
				model.put("email", email);
				model.put("token", token);
				return ok(model);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			
		} catch (AuthenticationException e) {
			throw new BadCredentialsException("Invalid username/password supplied");
		}
		
		return ok("This Account is not active");
	}	
	
	@SuppressWarnings("rawtypes")
	@PostMapping("/adminRegister")
	public ResponseEntity adminRegister(@RequestBody User admin) {
		
		System.out.println(admin.getPassword());
		
		  Roles asa=new Roles();
	      asa.setRole("ADMIN");
	      
	      Set setA = new HashSet();
	      setA.add(asa);
	      admin.setRoles(setA);
//		This findUserByUsernameOrEmailOrPhoneNo is used to check whether the users with email, username or phone number already exist or not
//		User userExists = userService.findUserByEmailOrPhoneNo(user.getEmail(),user.getContactnumber());
//		if (userExists != null) {
//			throw new BadCredentialsException("User with username, password or email: " + user.getUsername() + " already exists");
//		}
		userService.saveUser(admin);
		Map<Object, Object> model = new HashMap<>();
		model.put("message", "User registered successfully");
		model.put("status", "200");
		return ok(model);
	}
	
	
	//THis is for displaying the imgesof advertising
	 @RequestMapping(value = "/viewiageadvertising/{sid}", method = RequestMethod.GET)

	    public void getImage(HttpServletResponse response, @PathVariable ("sid") String mediatype) throws IOException {
		 
		 

//	        ClassPathResource imgFile = new ClassPathResource("/uploadingDir/"+mediatype);
//	        String filename = applyjobss;
//	        File imgFile = new File(filename);
		 
		 System.out.println(applyjobss);
		 
	        ClassPathResource imgFile = new ClassPathResource("/uploadingDir/"+mediatype);

	        
	        StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
	    }
	 
	 
	 
	 
	 
	//THis is for displaying the imgesof advertising
		 @RequestMapping(value = "/viewiageattachmemnt/{sid}", method = RequestMethod.GET)

		    public void viewiageattachmemnt(HttpServletResponse response, @PathVariable ("sid") String mediatype) throws IOException {

		        ClassPathResource imgFile = new ClassPathResource(rootPath+"/"+mediatype);

		       
		        
		        StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
		    }
		 
		 
		 
		 
//		THis is for displaying the imgesof advertising
		 @RequestMapping(value = "/AdveretisementMedia/{sid}", method = RequestMethod.GET)

		    public String viewiAdvertiseAttachment(HttpServletResponse response, @PathVariable ("sid") String mediatype) throws IOException {

			 System.getProperty("java.io.tmpdir");
			 
			 
		        ClassPathResource imgFile = new ClassPathResource("/home/bruce/Documents/Agile Group Projects/RuAin_AOS/uploadingDir"+mediatype);
				 System.getProperty("java.io.tmpdir");

		        String adfdsd=System.getProperty("java.io.tmpdir")+"/"+mediatype;
		        
		        return adfdsd;
		    }
		 
		 
//		 @RequestMapping(value = "/viewImages/{files}", method = RequestMethod.GET) 
//		   public ResponseEntity<Object> viewImages(@PathVariable("files") String files) throws IOException  {
//		      String filename = "/var/tmp/"+files;
//		      
//		      	System.out.println("dsffffffffffff");
//		      File file = new File(filename);
//		      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
//		      HttpHeaders headers = new HttpHeaders();
//		      
//		      headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
//		      headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
//		      headers.add("Pragma", "no-cache");
//		      headers.add("Expires", "0");
//		      
//		      ResponseEntity<Object> 
//		      responseEntity = ResponseEntity.ok().headers(headers).contentLength(
//		         file.length()).contentType(MediaType.MULTIPART_FORM_DATA).body(resource);
//		      
//		      return responseEntity;
//		   }
//		 
	
		   @RequestMapping(value = "/sid/{files}", method = RequestMethod.GET
		          )

		    public void getImadge(HttpServletResponse response,@PathVariable("files") String files) throws IOException {

			   
			      String filename = "/var/tmp/"+files;

//			   ClassPathResource imgFile = new ClassPathResource("image/sid.jpg");
			   
//			   File file = new File(filename);
			    InputStream targetStream = new FileInputStream(filename);
			   
//			   System.out.println(bytesArray);
//			      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
//		        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
//		        
		        StreamUtils.copy(targetStream, response.getOutputStream());
		    }
		 
		 
		 
		 @RequestMapping(value = "/download/{files}", method = RequestMethod.GET) 
		   public ResponseEntity<Object> downloadFile(@PathVariable("files") String files) throws IOException  {
		      String filename = "/var/tmp/"+files;
		      
		      	System.out.println("dsffffffffffff");
		      File file = new File(filename);
		      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		      HttpHeaders headers = new HttpHeaders();
		      
		      headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
		      headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		      headers.add("Pragma", "no-cache");
		      headers.add("Expires", "0");
		      
		      ResponseEntity<Object> 
		      responseEntity = ResponseEntity.ok().headers(headers).contentLength(
		         file.length()).contentType(MediaType.APPLICATION_PDF).body(resource);
		      
		      return responseEntity;
		   }
		 
		 
			//THis is for displaying the imgesof advertising
		 @RequestMapping(value = "/userapplyattchment/{sid}", method = RequestMethod.GET)

		    public void userapplyattchment(HttpServletResponse response, @PathVariable ("sid") String mediatype) throws IOException {

		        ClassPathResource imgFile = new ClassPathResource(rootPath+"/uploads/userapplyattchment/"+mediatype);

		        
		        StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
		    }
		 
		 
		 
		 
		 
		  @GetMapping("/getAllJobsDetailsById/{jobId}")
		    public ResponseEntity<Jobs> getAllJobsDetailsBYId(@PathVariable("jobId") String jobId ) {
			  
			  
		        return ResponseEntity.ok(jobService.findAllJobsById(jobId));

		        
		  }
		  
		  
		  @GetMapping("/getAllJObslistByLated")
		    public ResponseEntity<List<Jobs>> getAllJObslistByLated( ) {
			  
			  
		        return ResponseEntity.ok(jobService.findlatestjobsbynew());

		        
		  }
		  
		  @GetMapping("/getAllJObslistByWIthLImit/{paginationvale}")
		    public ResponseEntity<List<Jobs>> getAllJObslistByWIthLImit(@PathVariable("paginationvale") int pagevalue ) {
			  
			  
		        return ResponseEntity.ok(jobService.findAlltheLatestJobsWithLimit(pagevalue));

		        
		  }
		  
		  @GetMapping("/getALljobsss")
		    public ResponseEntity<List<Jobs>> getALljobsss( ) {
			  
			  
		        return ResponseEntity.ok(jobService.findAllJobs());

		        
		  }
		  
		  
		  @GetMapping("/getALljobsssWIthLimit/{pageinationumber}")
		    public ResponseEntity<List<Jobs>> getALljobsssWIthLimit(@PathVariable("pageinationumber") int jobin ) {
			  
			  
		        return ResponseEntity.ok(jobService.findAlltheLatestJobsWithLimit(jobin));

		        
		  }
		  
		  
		  
//		----------------------APply JObs -----------------------  
//		  @SuppressWarnings("rawtypes")
		  @PutMapping("/applyForJobs/{jobId}")
		  public ResponseEntity applyForJobs(Applicants applicants,@RequestParam("appa") String postSon,
		  		@PathVariable("jobId") String jobId,
		  		@RequestParam(value="appluserImages",required = false) MultipartFile filess) {

		  	try {
		  			
		  		applicants=new ObjectMapper().readValue(postSon, Applicants.class);

		  		  String returntype= jobService.applyForJobs(applicants, jobId,filess ,applyjobss);

		  			model.put("message", returntype);
		  			model.put("status", "y200");

		  		

		  	} catch (Exception e) {
		  		// TODO: handle exception
		  	}
		  	
		  	return ok(model);
		  }
		  
		  
		  
		  // This is for Seacrching a data
		  
		  @GetMapping("/searchTheJob/{searchtext}")
		    public ResponseEntity<List<Jobs>> searchTheJob(@PathVariable("searchtext") String searchtext ) {
			  
		        return ResponseEntity.ok(jobService.searchALltheJobs(searchtext));

		        
		  }
		  
		  
		  
		  
		  @GetMapping("/getAllAdvertisingLatest")
		    public ResponseEntity<List<AdvertisingModel>> getAllAdvertisingLatest( ) {
			  
			  
		        return ResponseEntity.ok(advertisementService.getAllAdvertisementBYLatest());

		        
		  }
		  
		  
		  
		  @GetMapping("/getAllAdvertisingLatestWithLimit/{value}")
		    public ResponseEntity<List<AdvertisingModel>> getAllAdvertisingLatestWithLimit(@PathVariable("value") int valuetype ) {
			  
			  
		        return ResponseEntity.ok(advertisementService.getAllAdvertisementBYLatestWIthSort(valuetype));

		        
		  }
		  
		  
		  
		  
	 


}