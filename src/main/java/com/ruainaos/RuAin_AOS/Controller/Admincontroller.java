package com.ruainaos.RuAin_AOS.Controller;

import static org.springframework.http.ResponseEntity.ok;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruainaos.RuAin_AOS.Models.AdvertisingModel;
import com.ruainaos.RuAin_AOS.Models.Applicants;
import com.ruainaos.RuAin_AOS.Models.Jobs;
import com.ruainaos.RuAin_AOS.Models.User;
import com.ruainaos.RuAin_AOS.Service.AdvertisementService;
import com.ruainaos.RuAin_AOS.Service.CustomUserDetailsService;
import com.ruainaos.RuAin_AOS.Service.JobService;


@CrossOrigin( origins = "*" )

@RestController
@RequestMapping("/adminApi/")
public class Admincontroller {
	
	@Autowired
	private CustomUserDetailsService userService;
	
//	String attachmentFilesPath="uploads/jobimageatachment";
//	String attachementmediapathforAdvertiseent="uploads/AdveretisementMedia";
	
	String attachmentFilesPath="/var/tmp/";
	
	String attachementmediapathforAdvertiseent="/var/tmp/";


	
    ClassPathResource imgFile = new ClassPathResource("jobimageatachment/");
//    String attachementmediapathforAdvertiseent=imgFile.toString();
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private AdvertisementService advertisementService;
	
	Map<Object, Object> model = new HashMap<>();


	
	@SuppressWarnings("rawtypes")
	 @GetMapping("/getAdminProfile")
	    public ResponseEntity<User> getALlUser(Authentication authentication ) {
		  
		  
	        return ResponseEntity.ok(userService.getAdminProfile(authentication.getName()));
	    }

	
	
	@SuppressWarnings("rawtypes")
	@PutMapping("/updateProfile")
	public ResponseEntity updateProfile(@RequestBody User admin,Authentication authentication) {
		

		boolean returnstatus= userService.updateUserProfile(admin, authentication.getName());
		Map<Object, Object> model = new HashMap<>();
		if(returnstatus==true) {
		
			model.put("message", "User Profile Update successfully");
			model.put("status", "200");
		}else
		{
			model.put("message", "User Profile Update Failed");
			model.put("status", "404");
		}
		
		return ok(model);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/updateEmails/{emails}")
	public ResponseEntity deleteTheJobById(Authentication authentication,@PathVariable("emails") String emailUsername) {
	
		try {
			
			
			String returntypes=userService.updateEmailorUsername(authentication.getName(), emailUsername);
			model.put("message", returntypes);
			model.put("status", "status");


			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return ok(model);
	}
	
	
	
	
	// THis APi routes basically Save the Job Post through it's Controller
	  @PostMapping("/postJobs")
	    public ResponseEntity postJobs(Jobs job,@RequestParam(value="jobAttachememnt",required=false) MultipartFile file,
	    		@RequestParam("job") String jobjson,
	    		 Authentication authentication
	    		
	    		) {
		  
		  
		  try {
			job=new ObjectMapper().readValue(jobjson, Jobs.class);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		  model.put("postJob", "Sucessfully Created Job Post");
		  
		  jobService.postJob(job, file, attachmentFilesPath);
		  
	        return ResponseEntity.ok("Sucessfully Job Posted");
	    }
	  
	  
	  
	// THis APi routes basically Save the Job Post through it's Controller
		  @PostMapping("/updateTheJobs/{jobId}")
		    public ResponseEntity updateTheJobsEceptAttachment(@RequestBody Jobs job,@PathVariable("jobId") String jobId,
		    	
		    		 Authentication authentication
		    		
		    		) {
			  
			  
			  
			  model.put("postJob", "Sucessfully Created Job Post");
			  
			 jobService.updateJobs(job, jobId);
			  
		        return ResponseEntity.ok("Sucessfully Job Posted");
		    }
		  
		  
		  @DeleteMapping("/deleteJobsAtachment/{jobId}")
		    public ResponseEntity deleteJobsAtachment(Authentication authentication,@PathVariable("jobId") String jobId ) {
			  
			  jobService.delteTheAttachment(jobId, attachmentFilesPath);
		        return ResponseEntity.ok("Sucesfully Attachment Deleted");

		        
		  }
		  
		  
		  
		  @GetMapping("/deleteTHeJobs/{jobId}")
		  public ResponseEntity deleteTHeJobs(Authentication authentication,@PathVariable("jobId") String jobId ) {
		  	jobService.deleteTheJobs(jobId, attachmentFilesPath);
		    
		      return ResponseEntity.ok("Sucessfully Deleted");

		      
		  }
		  
		  
		  
		  
			// THis APi routes basically Save the Job Post through it's Controller
		  @PostMapping("/updateTheAttachment/{jobId}")
		    public ResponseEntity updateTheAttachment(@RequestParam("jobAttachememnt") MultipartFile file,
		    	
		    		 Authentication authentication,@PathVariable("jobId") String jobId
		    		
		    		) {
			  
			
			  
			  jobService.updateTheAttachment(jobId, attachmentFilesPath, file);
			  
		        return ResponseEntity.ok("Sucessfully Job Attachment Updated");
		    }
		  
		  

		  
		  
	  
	  
	  @GetMapping("/getAllJobsDetails")
	    public ResponseEntity<List<Jobs>> getAllJobsDetails(Authentication authentication ) {
		  
		  
	        return ResponseEntity.ok(jobService.findAllJobs());

	        
	  }
	  
	  
	  
	

	  
	  
	  
	  

	  
	  
	  
	  
	  //Advertisement SEction
	  
	  

		// THis APi routes basically Save the Job Post through it's Controller
		  @PostMapping("/postAdvetisement")
		    public ResponseEntity postAdvetisement(AdvertisingModel advertisingModel,
		    		@RequestParam(value=("advertisingMedia"),required=false) MultipartFile file,
		    		@RequestParam("advertisement") String jobjson,
		    		 Authentication authentication
		    		
		    		) {
			  
			  
			  try {
				  advertisingModel=new ObjectMapper().readValue(jobjson, AdvertisingModel.class);
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  
			  model.put("postJob", "Sucessfully Created Job Post");
			  
			  advertisementService.postAdvertisement(advertisingModel, file, attachementmediapathforAdvertiseent);
			  
		        return ResponseEntity.ok("Sucessfully Advertisement Posted");
		    }
		  
		  
		  // THis for getting all the Advertiement
		  @SuppressWarnings("rawtypes")
			 @GetMapping("/getALlAdvertiseent")
			    public ResponseEntity<List<AdvertisingModel>> getALlAdvertiseent(Authentication authentication ) {
 
			        return ResponseEntity.ok(advertisementService.getAllAdvertisement());
			    }



// THis for getting all the Advertiement By Id
@SuppressWarnings("rawtypes")
	 @GetMapping("/getALlAdvertiseent/{advertisememntId}")
	    public ResponseEntity<AdvertisingModel> getALlAdvertiseentBySpecificone(Authentication authentication
	    		,@PathVariable("advertisememntId") String advertisememntId
	    		) {

	        return ResponseEntity.ok(advertisementService.getAdvertisementById(advertisememntId));
	    }




//THis APi routes basically Updaate The Advertisement
@PutMapping("/updateAdvetisement/{advertisemenId}")
  public ResponseEntity updateTheAdvertiseentExceptMedia(@RequestBody AdvertisingModel advertisingModel,
		  @PathVariable("advertisemenId") String advertisemenId,
  		 Authentication authentication	
  		) {
	
	System.out.println("fsdfsdf!!!!!!!!");
	System.out.println(advertisingModel);

	  model.put("postJob", "Sucessfully Created Job ADVERTISEMENT");
	  advertisementService.updateAdvertisementExceptMedia(advertisingModel, advertisemenId);
	  
	  
      return ResponseEntity.ok("Sucessfully Advertisement Update");
  }



// THis APi routes basically Save the Job Post through it's Controller
@PostMapping("/updateTheAttachmentForAdvertisement/{advertiseentId}")
  public ResponseEntity updateTheAttachmentForAdvertisement(@RequestParam("advertisingMedia") MultipartFile file,
  	
  		 Authentication authentication,@PathVariable("advertiseentId") String advertiseentId
  		
  		) {
	  
	
	  
	  advertisementService.updateAdvertisementMedia(advertiseentId, attachementmediapathforAdvertiseent, file);
	  
      return ResponseEntity.ok("Sucessfully Job Attachment Updated");
  }




@GetMapping("/deleteAdvertisementMedia/{advertiseentId}")
public ResponseEntity deleteAdvertisementMedia(Authentication authentication,@PathVariable("advertiseentId") String jobId ) {
	advertisementService.deletetheAdvertisingMedia(jobId, attachementmediapathforAdvertiseent);
  
    return ResponseEntity.ok("Sucessfully Deleted");

    
}

@DeleteMapping("/deleteAdvertisementMediaAll/{advertiseentId}")
public ResponseEntity deleteAdvertisementMediaAll(Authentication authentication,@PathVariable("advertiseentId") String jobId ) {
	advertisementService.deletetheAdvertisingMediaAll(jobId, attachementmediapathforAdvertiseent);
  
    return ResponseEntity.ok("Sucessfully Deleted");

    
}












}





