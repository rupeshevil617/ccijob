package com.ruainaos.RuAin_AOS.Models;

public class Applicants {
	
	
	private String applicantid;
	
	private String fullname;
	
	private String email;
	
	private String phone;
	
	private String  gender;
	
	private String cv;
	
	private String cover;
	
	@Override
	public String toString() {
		return "Applicants [applicantid=" + applicantid + ", fullname=" + fullname + ", email=" + email + ", phone="
				+ phone + ", gender=" + gender + ", cv=" + cv + ", cover=" + cover + ", city=" + city + ", district="
				+ district + ", streetaddress=" + streetaddress + "]";
	}

	private String city;
	
	private String district;
	
	private String streetaddress;

	public String getApplicantid() {
		return applicantid;
	}

	public void setApplicantid(String applicantid) {
		this.applicantid = applicantid;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getStreetaddress() {
		return streetaddress;
	}

	public void setStreetaddress(String streetaddress) {
		this.streetaddress = streetaddress;
	}
	
	

}
