package com.ruainaos.RuAin_AOS.Models;

public class Companyinfo {
	
	private String companyname;
	
	private String phonenumber;
	
	private String email;
	
	private String companyinfo;
	
	private String companycity;
	
	private String companydistrict;
	
	private String streetaddress;

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyinfo() {
		return companyinfo;
	}

	public void setCompanyinfo(String companyinfo) {
		this.companyinfo = companyinfo;
	}

	public String getCompanycity() {
		return companycity;
	}

	public void setCompanycity(String companycity) {
		this.companycity = companycity;
	}

	public String getCompanydistrict() {
		return companydistrict;
	}

	public void setCompanydistrict(String companydistrict) {
		this.companydistrict = companydistrict;
	}

	public String getStreetaddress() {
		return streetaddress;
	}

	public void setStreetaddress(String streetaddress) {
		this.streetaddress = streetaddress;
	}

	@Override
	public String toString() {
		return "Companyinfo [companyname=" + companyname + ", phonenumber=" + phonenumber + ", email=" + email
				+ ", companyinfo=" + companyinfo + ", companycity=" + companycity + ", companydistrict="
				+ companydistrict + ", streetaddress=" + streetaddress + "]";
	}
	

}
