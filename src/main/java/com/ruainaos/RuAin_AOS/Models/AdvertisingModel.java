package com.ruainaos.RuAin_AOS.Models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "advertisement")

public class AdvertisingModel {
	
	@Id
	private String advertiseid;
	
	public String getAdvertiseid() {
		return advertiseid;
	}

	public void setAdvertiseid(String advertiseid) {
		this.advertiseid = advertiseid;
	}

	@Override
	public String toString() {
		return "AdvertisingModel [advertiseid=" + advertiseid + ", advertisetext=" + advertisetext + ", advertisemedia="
				+ advertisemedia + ", advertiselink=" + advertiselink + ", postedate=" + postedate + "]";
	}

	public String getAdvertisetext() {
		return advertisetext;
	}

	public void setAdvertisetext(String advertisetext) {
		this.advertisetext = advertisetext;
	}

	public String getAdvertisemedia() {
		return advertisemedia;
	}

	public void setAdvertisemedia(String advertisemedia) {
		this.advertisemedia = advertisemedia;
	}

	public String getAdvertiselink() {
		return advertiselink;
	}

	public void setAdvertiselink(String advertiselink) {
		this.advertiselink = advertiselink;
	}

	private  String advertisetext;
	
	private String advertisemedia;
	
	private String advertiselink;
	
	private Date postedate;

	public Date getPostedate() {
		return postedate;
	}

	public void setPostedate(Date postedate) {
		this.postedate = postedate;
	}
	
	

}
