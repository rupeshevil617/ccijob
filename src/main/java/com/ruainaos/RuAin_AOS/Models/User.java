package com.ruainaos.RuAin_AOS.Models;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "users")

public class User {
	
	@Id
	private String userid;
	
	private String fullname;
		
	private String contactnumber;
	
	private String email;
    
    private String password;
    

	@DBRef
	 private Set<Roles> roles;
    
private String city;

@Override
public String toString() {
	return "User [userid=" + userid + ", fullname=" + fullname + ", contactnumber=" + contactnumber + ", email=" + email
			+ ", password=" + password + ", roles=" + roles + ", city=" + city + ", district=" + district
			+ ", streetaddress=" + streetaddress + ", enabled=" + enabled + ", accountcreationdate="
			+ accountcreationdate + "]";
}

public String getUserid() {
	return userid;
}

public void setUserid(String userid) {
	this.userid = userid;
}

public String getFullname() {
	return fullname;
}

public void setFullname(String fullname) {
	this.fullname = fullname;
}

public String getContactnumber() {
	return contactnumber;
}

public void setContactnumber(String contactnumber) {
	this.contactnumber = contactnumber;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public Set<Roles> getRoles() {
	return roles;
}

public void setRoles(Set<Roles> roles) {
	this.roles = roles;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public String getDistrict() {
	return district;
}

public void setDistrict(String district) {
	this.district = district;
}

public String getStreetaddress() {
	return streetaddress;
}

public void setStreetaddress(String streetaddress) {
	this.streetaddress = streetaddress;
}

public boolean isEnabled() {
	return enabled;
}

public void setEnabled(boolean enabled) {
	this.enabled = enabled;
}

public LocalDateTime getAccountcreationdate() {
	return accountcreationdate;
}

public void setAccountcreationdate(LocalDateTime accountcreationdate) {
	this.accountcreationdate = accountcreationdate;
}

private String district;

private String streetaddress;

private boolean enabled;

private LocalDateTime accountcreationdate;


}
