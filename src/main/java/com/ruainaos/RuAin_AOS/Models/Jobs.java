package com.ruainaos.RuAin_AOS.Models;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "jobs")

public class Jobs {
	
	@Id
	private String jobid;
	
	@Override
	public String toString() {
		return "Jobs [jobid=" + jobid + ", jobtitle=" + jobtitle + ", jobtypes=" + jobtypes + ", jobcategories="
				+ jobcategories + ", jobdescription=" + jobdescription + ", criteria=" + criteria + ", appliedusers="
				+ appliedusers + ", requirnmentlist=" + requirnmentlist + ", posteddate=" + posteddate
				+ ", jobdeadline=" + jobdeadline + ", jobimages=" + jobimages + ", jobattachment=" + jobattachment
				+ ", salary=" + salary + ", companyinfo=" + companyinfo + "]";
	}


	private String jobtitle;
	
	private String jobtypes;
	
	private String jobcategories;
	
	private String jobdescription;
	
	private List<String> criteria;
	
	private List<Applicants> appliedusers;
	
	public List<Applicants> getAppliedusers() {
		return appliedusers;
	}


	public void setAppliedusers(List<Applicants> appliedusers) {
		this.appliedusers = appliedusers;
	}


	public List<String> getCriteria() {
		return criteria;
	}


	public void setCriteria(List<String> criteria) {
		this.criteria = criteria;
	}


	private List<String> requirnmentlist;
	
	public List<String> getRequirnmentlist() {
		return requirnmentlist;
	}


	public void setRequirnmentlist(List<String> requirnmentlist) {
		this.requirnmentlist = requirnmentlist;
	}


	private Date posteddate;
	
	private Date jobdeadline;
	
	public Date getJobdeadline() {
		return jobdeadline;
	}


	public void setJobdeadline(Date jobdeadline) {
		this.jobdeadline = jobdeadline;
	}


	private String jobimages;
	
	private String jobattachment;
		
	private String salary;
	
	
	



	public String getJobid() {
		return jobid;
	}


	public void setJobid(String jobid) {
		this.jobid = jobid;
	}


	public String getJobtitle() {
		return jobtitle;
	}


	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}


	public String getJobtypes() {
		return jobtypes;
	}


	public void setJobtypes(String jobtypes) {
		this.jobtypes = jobtypes;
	}


	public String getJobcategories() {
		return jobcategories;
	}


	public void setJobcategories(String jobcategories) {
		this.jobcategories = jobcategories;
	}


	public String getJobdescription() {
		return jobdescription;
	}


	public void setJobdescription(String jobdescription) {
		this.jobdescription = jobdescription;
	}


	public Date getPosteddate() {
		return posteddate;
	}


	public void setPosteddate(Date posteddate) {
		this.posteddate = posteddate;
	}


	public String getJobimages() {
		return jobimages;
	}


	public void setJobimages(String jobimages) {
		this.jobimages = jobimages;
	}


	public String getJobattachment() {
		return jobattachment;
	}


	public void setJobattachment(String jobattachment) {
		this.jobattachment = jobattachment;
	}


	public String getSalary() {
		return salary;
	}


	public void setSalary(String salary) {
		this.salary = salary;
	}


	


	public Companyinfo getCompanyinfo() {
		return companyinfo;
	}


	public void setCompanyinfo(Companyinfo companyinfo) {
		this.companyinfo = companyinfo;
	}


	private Companyinfo companyinfo;
	
	
	
	
	
	

}
