package com.ruainaos.RuAin_AOS.Security;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class AdditionalResourceWebConfiguration implements WebMvcConfigurer{
	
	
	@Override
	  public void addResourceHandlers(final ResourceHandlerRegistry registry) {
	    registry.addResourceHandler("/asda/**").addResourceLocations("/asda/");
	  }
	

}
