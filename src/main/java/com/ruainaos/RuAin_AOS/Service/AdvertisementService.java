package com.ruainaos.RuAin_AOS.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ruainaos.RuAin_AOS.Models.AdvertisingModel;
import com.ruainaos.RuAin_AOS.Models.Jobs;
import com.ruainaos.RuAin_AOS.Repositories.AdvertisementRepositories;



@Service
public class AdvertisementService {
	
	@Autowired
	private AdvertisementRepositories advertisementRepositories;
	
	@Autowired
	private Custommethod custommethod;
	
	@Autowired
	private MongoOperations mongoOperations;
	
	
	
	public void postAdvertisement(AdvertisingModel advertisingModel,MultipartFile attachment,String path) {
		
		if(attachment !=null) {
		
			String returnAttachmentFiles= custommethod.saveanFilesandReeturnsName(attachment, path);
			
			advertisingModel.setAdvertisemedia(returnAttachmentFiles);
			
			System.out.println(returnAttachmentFiles);
		}
	
		
		advertisingModel.setPostedate(new Date());
		
		advertisementRepositories.save(advertisingModel);
	
	}
	
	// This is for getting ALl the advertisement
	
	public List<AdvertisingModel> getAllAdvertisement(){
		
		return advertisementRepositories.findAll();
		
	}
	
	
	// This is for getting ALl the advertisement
	
		public List<AdvertisingModel> getAllAdvertisementBYLatest(){
			
			List<AdvertisingModel> passengers = advertisementRepositories.findAll(Sort.by(Sort.Direction.DESC, "postedate"));

			return passengers;

		}
		
		public List<AdvertisingModel> getAllAdvertisementBYLatestWIthSort(int value){
			
			List<AdvertisingModel> passengers = advertisementRepositories.findAll(Sort.by(Sort.Direction.DESC, "postedate"));

			
			 List<AdvertisingModel> dsfsdfsdfaa=new ArrayList<AdvertisingModel>();
			 
			 for (int i = value; i < passengers.size(); i++) {
				 

				 dsfsdfsdfaa.add(passengers.get(i));
				  
		         
		      }
			 
			 
			 
			return dsfsdfsdfaa;
			
			
		}

	
	// THis is for getting the advertisement By Specific Id
	public AdvertisingModel getAdvertisementById(String id) {
		
		return advertisementRepositories.findById(id).get();
	}
	
	
	// THis is for updating the Advertisement Except Media
	public void updateAdvertisementExceptMedia(AdvertisingModel advertisingModel,String id) {
		
		AdvertisingModel  advert2=advertisementRepositories.findById(id).get();
		
		if(advert2.getAdvertisemedia() !=null) {
		
			advertisingModel.setAdvertisemedia(advert2.getAdvertisemedia());

		}
		
		advertisingModel.setPostedate(advert2.getPostedate());
		advertisingModel.setAdvertiseid(id);
		
		advertisementRepositories.save(advertisingModel);
		
	}
	
	
	// This is for updating the  Advertising Media
	public void updateAdvertisementMedia(String id, String path,MultipartFile filestoupdate) {
		
		AdvertisingModel sadsa=advertisementRepositories.findById(id).get();
		if(sadsa.getAdvertisemedia() !=null) {
			custommethod.deletetheImages(sadsa.getAdvertisemedia(), path);

			
		}
		
		
		String returnname=custommethod.saveanFilesandReeturnsName(filestoupdate, path);
		
		Query query = new Query();
		query.addCriteria(Criteria.where("advertiseid").is(id));
		
		Update update = new Update();
		update.set("advertisemedia", returnname);
		
		
	 mongoOperations.findAndModify(
			 query, update, 
				new FindAndModifyOptions().returnNew(true), AdvertisingModel.class);
		

		
	}
	
	// THis is for deleting the Advertising Media
	public void deletetheAdvertisingMedia(String id,String path) {
		
		AdvertisingModel sadsa=advertisementRepositories.findById(id).get();
		if(sadsa.getAdvertisemedia() !=null) {
			custommethod.deletetheImages(sadsa.getAdvertisemedia(), path);	
		}
		
		Query query = new Query();
		query.addCriteria(Criteria.where("advertiseid").is(id));
		
		Update update = new Update();
		update.set("advertisemedia", null);
		
		
	 mongoOperations.findAndModify(
			 query, update, 
				new FindAndModifyOptions().returnNew(true), AdvertisingModel.class);
		
		
		
	}
	
	
	// THis is for deleting the Advertising Media
		public void deletetheAdvertisingMediaAll(String id,String path) {
			
			AdvertisingModel sadsa=advertisementRepositories.findById(id).get();
			if(sadsa.getAdvertisemedia() !=null) {
				custommethod.deletetheImages(sadsa.getAdvertisemedia(), path);	
			}
			
			
			advertisementRepositories.deleteById(id);
			
			
			
		}
		
	
	
	
	

}
