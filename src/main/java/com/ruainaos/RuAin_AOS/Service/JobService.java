package com.ruainaos.RuAin_AOS.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ruainaos.RuAin_AOS.Models.Applicants;
import com.ruainaos.RuAin_AOS.Models.Jobs;
import com.ruainaos.RuAin_AOS.Repositories.JobRepositories;



@Service
public class JobService {
	
	@Autowired 
	private JobRepositories jobRepositories;
	
	@Autowired
	private Custommethod custommethod;
	
	@Autowired
	private MongoOperations mongoOperations;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	
	
	
	public void postJob(Jobs jobs,MultipartFile attachment,String path) {
		
		if(attachment !=null) {
			
			String returnAttachmentFiles= custommethod.saveanFilesandReeturnsName(attachment, path);
			jobs.setJobattachment(returnAttachmentFiles);

			
		}
		
		
		jobs.setPosteddate(new Date());
		
		jobRepositories.save(jobs);
	
	}
	
	
	
	
	public List<Jobs> findlatestjobsbynew(){
		
		List<Jobs> passengers = jobRepositories.findAll(Sort.by(Sort.Direction.DESC, "posteddate"));
		
		
		return passengers;
	}
	
//	public List<Jobs> findlatestjobsbynewWithPagination(int page){
//		
//		final Pageable pageableRequest = PageRequest.of(page, 2);
//		List<Jobs> passengers = jobRepositories.findAll(pageableRequest,Sort.by(Sort.Direction.DESC, "posteddate"));
//		
//		
//		return passengers;
//	}
//	
	
	
public List<Jobs> findAlltheLatestJobsWithLimit(int value){
		
//		List<Jobs> passengers = jobRepositories.findAll(Sort.by(Sort.Direction.DESC, "posteddate"));
		
		Query query = new Query();
		query.with(Sort.by(Sort.Direction.DESC, "posteddate"));
//		query.with(pageableRequest).with();
		
		
		
	
		
		 List<Jobs> dfsdfsd=mongoOperations.find(query, Jobs.class);
		 
		 List<Jobs> dsfsdfsdfaa=new ArrayList<Jobs>();
		 
		 for (int i = value; i < dfsdfsd.size(); i++) {
			 

			 dsfsdfsdfaa.add(dfsdfsd.get(i));
			  
	         
	      }
		 
		 
		 
		return dsfsdfsdfaa;

		
	}


	
//	THis is for deleting the jobs
	
	public void updateJobs(Jobs jobs,String id) {
		
		System.out.println(jobs.toString());
Jobs jobsreturn=jobRepositories.findById(id).get();


jobs.setPosteddate(jobsreturn.getPosteddate());
if(jobsreturn.getAppliedusers() !=null) {
	jobs.setAppliedusers(jobsreturn.getAppliedusers());

	
}
		jobs.setPosteddate(new Date());
		
		jobs.setJobid(id);
		
		jobRepositories.save(jobs);
	
	}
	
	public List<Jobs> findAllJobs(){
		
		return jobRepositories.findAll();
		
		
	}
	
		public Jobs findAllJobsById(String id){
		
		return jobRepositories.findById(id).get();
		
		
	}
		
		// This is for deleting the jobsAttachements
		
		public void delteTheAttachment(String id,String path){
			
		Jobs asd= jobRepositories.findById(id).get();
		
		custommethod.deletetheImages(asd.getJobattachment(), path);
			
		Query query = new Query();
		query.addCriteria(Criteria.where("jobid").is(id));
		
		Update update = new Update();
		update.set("jobattachment", null);
		
		//FindAndModifyOptions().returnNew(true) = newly updated document
		//FindAndModifyOptions().returnNew(false) = old document (not update yet)
	 mongoOperations.findAndModify(
			 query, update, 
				new FindAndModifyOptions().returnNew(true), Jobs.class);
			
		}
		
		
		// THis is for deleing the Jobs
		
		public void deleteTheJobs(String id, String path) {
		
			Jobs sadsa=jobRepositories.findById(id).get();
			custommethod.deletetheImages(sadsa.getJobattachment(), path);
			jobRepositories.deleteById(id);

			
		}
		
		
		public void updateTheAttachment(String id, String path,MultipartFile filestoupdate) {
			
			Jobs sadsa=jobRepositories.findById(id).get();
			
			
			custommethod.deletetheImages(sadsa.getJobattachment(), path);
			
			String returnname=custommethod.saveanFilesandReeturnsName(filestoupdate, path);
			
			Query query = new Query();
			query.addCriteria(Criteria.where("jobid").is(id));
			
			Update update = new Update();
			update.set("jobattachment", returnname);
			
			
		 mongoOperations.findAndModify(
				 query, update, 
					new FindAndModifyOptions().returnNew(true), Jobs.class);
			

			
		}
		
		
		public List<Jobs> searchALltheJobs(String searchvalue){
		
			
			Query query = new Query();
			
				new Criteria();
				Criteria cr1 = Criteria.where("jobtitle").regex(searchvalue, "i");
				new Criteria();
				
				Criteria cr2 = Criteria.where("jobtypes").regex(searchvalue, "i");
				new Criteria();
				Criteria cr3 = Criteria.where("jobcategories").regex(searchvalue, "i");
				new Criteria();
				Criteria cr5 = Criteria.where("jobdescription").regex(searchvalue, "i");
				
				
				new Criteria();
				Criteria cr6 = Criteria.where("companyinfo.companyname").regex(searchvalue,"i");
				new Criteria();
				Criteria cr7 = Criteria.where("companyinfo.phonenumber").regex(searchvalue,"i");
				new Criteria();
				Criteria cr8 = Criteria.where("companyinfo.email").regex(searchvalue,"i");
				new Criteria();
				Criteria cr9 = Criteria.where("companyinfo.companyinfo").regex(searchvalue,"i");
				new Criteria();
				Criteria cr10 = Criteria.where("companyinfo.companycity").regex(searchvalue,"i");
				new Criteria();
				Criteria cr11 = Criteria.where("companyinfo.companydistrict").regex(searchvalue,"i");
				new Criteria();
				Criteria cr12 = Criteria.where("companyinfo.streetaddress").regex(searchvalue,"i");
				new Criteria();
				Criteria cr13 = Criteria.where("salary").regex(searchvalue,"i");
				
			 
					
				
				query.addCriteria(new Criteria().orOperator(cr1,cr2,cr3,cr5,cr6,cr7,cr8,cr9,cr10,cr11,cr12,cr13));
				
				 List<Jobs> dfsdfsd=mongoOperations.find(query, Jobs.class);

			return dfsdfsd;
			
		}
		
		
		
		// THis method will apply the users for jobs
		public String applyForJobs(Applicants applicantsoriginal,String jobId,MultipartFile multiparts, String FIlespath) {
			
			if(multiparts !=null) {
				String imagessdsad=custommethod.saveanFilesandReeturnsName(multiparts, FIlespath);
				
				applicantsoriginal.setCv(imagessdsad);
			}
			
			
			
			String returntype="";
			// THis will find the whether there is already an user apllied for this job or not
			
//			Jobs job=jobRepositories.findByJobIdAndJobApplicationListJobApplyerUsername(jobId, username);
			
			Jobs job=jobRepositories.findByJobidAndAppliedusersEmail(jobId,applicantsoriginal.getEmail());
			
			Jobs jobss=jobRepositories.findByJobidAndJobdeadlineGreaterThan(jobId,new Date());

//		Jobs jobss=jobRepositories.findByJobidAndAppliedusersEmailAndJobdeadlineBetween(jobId,applicantsoriginal.getEmail(),job.getJobdeadline(),new Date());



				Applicants applicationList=new Applicants();
				
				List<Applicants> applicationListsnew=new ArrayList();

//				If it return no value then job has not applied yet
				
				
//				if(jobrecheck.getJobid() !=null) {
					
					
				System.out.println(jobss);
					
					
				
				if(jobss !=null) {
					
					
				
					
				if(job ==null) {
					Jobs secJob=jobRepositories.findById(jobId).get();
					
						
					if(secJob.getAppliedusers()==null) {
						
					
						applicationListsnew.add(applicantsoriginal);
					
						secJob.setAppliedusers(applicationListsnew);
						
						jobRepositories.save(secJob);
						
						returntype="Applied";
						
						}else{
						
							applicationListsnew=secJob.getAppliedusers();	
							
					
						
						applicationListsnew.add(applicantsoriginal);	
						
						secJob.setAppliedusers(applicationListsnew);
						jobRepositories.save(secJob);	
						
						returntype="Applied";


						
					}
					




					
				}else {
					System.out.println("cannot applied for this job");
					returntype="Cannot applied for Job";
				
			
				
				}
				
				
				
				
				
				}
				
				
				
				else {
					
					returntype="Cannot applied for Job, you have passed deadline";
	
				}
				
				
				
				
				
// }else {
//	 
//		returntype="Cannot applied for Job, you have passed deadline";
// 
// }
// 
 


			
			return returntype;
			
		}
		
		
		public Jobs findJobsByAppieldId(String jobid, String useremail){
		
			Jobs job=jobRepositories.findByJobidAndAppliedusersEmail(jobid,useremail);

			return job;
		}
		
		
		
	
		
		

}
