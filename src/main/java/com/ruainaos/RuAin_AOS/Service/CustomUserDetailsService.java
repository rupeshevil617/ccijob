package com.ruainaos.RuAin_AOS.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.ruainaos.RuAin_AOS.Models.Roles;
import com.ruainaos.RuAin_AOS.Models.User;
import com.ruainaos.RuAin_AOS.Repositories.RoleRepository;
import com.ruainaos.RuAin_AOS.Repositories.UserRepository;





@Service

public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;
	
//	@Autowired
//	private TokenandMail tokenandMail;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private MongoOperations mongoOperations;
	
	@Autowired
	private MongoConverter mongoConverter;

	
	
	
	 private DBObject getDbObject(Object o) {
	        BasicDBObject basicDBObject = new BasicDBObject();
	        mongoConverter.write(o, basicDBObject);
	        return basicDBObject;
	    }
	
	
	
	
//	public User findUserByEmail(String email) {
//	    return userRepository.findByEmail(email);
//	}
	public void saveUser(User user) {
		
		LocalDateTime localDate = LocalDateTime.now();
		user.setAccountcreationdate(localDate);
	
		
	    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
	    user.setEnabled(true);
	    Roles userRole = roleRepository.findByRole("ADMIN");
	    user.setRoles(new HashSet<>(Arrays.asList(userRole)));

	   user= userRepository.save(user);
		
	  
	    
	    
	}
	
public boolean updateUserProfile(User userdata,String email) {
		
		
	User user=userRepository.findByEmail(email);
	
	userdata.setRoles(user.getRoles());
	userdata.setAccountcreationdate(user.getAccountcreationdate());
	userdata.setUserid(user.getUserid());
	userdata.setPassword(user.getPassword());	
	 userdata.setEmail(user.getEmail());

	   user= userRepository.save(userdata);
		if(user !=null) {
			
			return true;
		}else {
			
			return false;
		}
	  
	    
	    
	}

public User getAdminProfile(String email) {
	
	
	User user=userRepository.findByEmail(email);
	
	return user;
   
	}
	
	
	
public String updateEmailorUsername(String email, String emailUsername) {
		
		
	User user=userRepository.findByEmail(email);
		
	if(user !=null) {
		
	
		return "This email is already Taken" ;
	}
	else {
		
		 user.setEmail(emailUsername);
		 

		   user= userRepository.save(user);
		   
		   return "Your Email is Sucessfull Updated";
		
	}
	   
	  
	    
	    
	}
	

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

	    User user = userRepository.findByEmail(email);
	    if(user != null) {
	        List<GrantedAuthority> authorities = getUserAuthority(user.getRoles());
	        return buildUserForAuthentication(user, authorities);
	    } else {
	        throw new UsernameNotFoundException("username not found");
	    }
	}
	

	private List<GrantedAuthority> getUserAuthority(Set<Roles> userRoles) {
	    Set<GrantedAuthority> roles = new HashSet<>();
	    userRoles.forEach((role) -> {
	        roles.add(new SimpleGrantedAuthority(role.getRole()));
	    });

	    List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
	    return grantedAuthorities;
	}
	
	private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
	    return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), authorities);
	}





	
}