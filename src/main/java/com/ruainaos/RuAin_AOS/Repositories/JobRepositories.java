package com.ruainaos.RuAin_AOS.Repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ruainaos.RuAin_AOS.Models.Jobs;


@Repository
public interface JobRepositories extends MongoRepository<Jobs, String>{

	Jobs findByJobidAndAppliedusersEmail(String jobId, String email);

//	Jobs findByJobidAndAppliedusersEmailAndJobdeadlineGt(String jobId, String email, Date date);

	Jobs findByJobidAndAppliedusersEmailAndJobdeadlineGreaterThan(String jobId, String email, Date date);

	Jobs findByJobidAndAppliedusersEmailAndJobdeadlineBetween(String jobId, String email, Date jobdeadline, Date date);

	Jobs findByJobidAndJobdeadlineGreaterThanAndAppliedusersEmail(String jobId, Date date, String email);

	Jobs findByJobidAndJobdeadlineGreaterThan(String jobId, Date date);



//	Jobs findByJobidAndAppliedusersEmailAndJobdeadlineGreaterThan(String jobId, String email, Date date);

	

}
