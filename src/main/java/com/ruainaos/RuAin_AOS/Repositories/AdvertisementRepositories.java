package com.ruainaos.RuAin_AOS.Repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ruainaos.RuAin_AOS.Models.AdvertisingModel;


@Repository
public interface AdvertisementRepositories extends MongoRepository<AdvertisingModel, String>{
	
	

}
