package com.ruainaos.RuAin_AOS.Repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ruainaos.RuAin_AOS.Models.Roles;


@Repository
public interface RoleRepository extends MongoRepository<Roles, String>{

	Roles findByRole(String string);

}
