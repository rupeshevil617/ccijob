package com.ruainaos.RuAin_AOS.Repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ruainaos.RuAin_AOS.Models.Jobs;

@Repository
public interface JobsRepositoriesPaging extends PagingAndSortingRepository<Jobs, String>{
	

}
