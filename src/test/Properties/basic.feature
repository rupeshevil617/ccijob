#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Login API

  Background: Given Validation and result test

  Scenario Outline: login with credentials i.e Username and Password
    Given Entering credentials
    And Checking login with Username "<username>" and Password "<password>"
    Then The output of login should be ok with response is "9"

  Examples:
    | username | password |
    | 5    | 4    |